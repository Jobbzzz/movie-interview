import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // Handle HTTP Errors.
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // console.log(error);
    return throwError(error.error);
  }


  getMoviesDetails(pageNumber: any): Observable<any> {
    const url = "https://api.themoviedb.org/3/movie/popular?api_key=1977f01e8a32e238e752af6c95e08cc9&language=en-US&page=" + pageNumber
    return this.http.get(url, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getMoviesDetailsbyID(id: any): Observable<any> {
    const url = "https://api.themoviedb.org/3/movie/" + id + "?api_key=1977f01e8a32e238e752af6c95e08cc9&language=en-US"
    return this.http.get(url, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  searchMovie(strSearch: any): Observable<any> {
    const url = "https://api.themoviedb.org/3/search/movie?api_key=1977f01e8a32e238e752af6c95e08cc9&language=en-US&query=" + strSearch + "&page=1&include_adult=false"
    return this.http.get(url, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
}
