import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HomeService } from 'src/app/service/home.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit {
  intMovieId: any;
  arrMovie: any = [];
  isLoading: boolean = true

  constructor(private route: ActivatedRoute, private movieService: HomeService, private location: Location) {
    this.intMovieId = this.route.snapshot.queryParamMap.get('movieId');
  }

  ngOnInit(): void {
    this.getMovieById(this.intMovieId);
  }

  getMovieById(id: any) {
    this.movieService.getMoviesDetailsbyID(id).subscribe(response => {
      this.arrMovie = response
      this.isLoading = false
    })
  }

  onClickBack() {
    this.location.back();
  }

}
