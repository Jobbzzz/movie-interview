import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailModuleRoutingModule } from './detail-module-routing.module';
import { DetailsPageComponent } from './details-page/details-page.component';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    DetailsPageComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    DetailModuleRoutingModule
  ]
})
export class DetailModuleModule { }
