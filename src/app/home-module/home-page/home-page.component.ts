import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from 'src/app/service/home.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  strSearch: any;
  arrMovie: any = [];
  arrPagination: any = [];
  intActivePage: any = 1;
  isLoading: boolean = true;
  intTotalPage: number = 0;

  constructor(private movieService: HomeService, private router: Router) { }

  ngOnInit(): void {
    this.getMovie();
  }

  getMovie() {
    this.movieService.getMoviesDetails(this.intActivePage).subscribe(response => {
      this.arrMovie = response;
      this.intActivePage = this.arrMovie.page;
      this.intTotalPage = this.arrMovie.total_pages;
      this.arrPagination = this.setPagination(this.intTotalPage, this.intActivePage);
      console.log(this.arrPagination, this.intTotalPage, this.intActivePage)
      this.isLoading = false
    })
  }

  setPagination(intTotalPage: number, intCurrentPage: number, intPageCount: number = 10) {
    const arrPages = [];
    if (intCurrentPage < 5) {
      for (let i = 1; i < intPageCount; i++) {
        arrPages.push(i);
      }
    } else if ((intCurrentPage + 10) < intTotalPage) {
      for (let i = intCurrentPage - 5; i < (intCurrentPage + 5); i++) {
        arrPages.push(i);
      }
    }
    return arrPages;
  }


  onClickPage(pageNumber: number): void {
    if (pageNumber >= 1 && pageNumber <= this.arrMovie.total_pages) {
      this.isLoading = true;
      this.intActivePage = pageNumber;
      this.getMovie();
    }
  }

  onClickSearch() {
    if (this.strSearch.trim()) {
      this.isLoading = true
      this.movieService.searchMovie(this.strSearch).subscribe(response => {
        this.arrPagination = [];
        this.arrMovie = response;
        this.intActivePage = this.arrMovie.page;
        this.intTotalPage = this.arrMovie.total_pages;
        this.arrPagination = this.setPagination(this.intTotalPage, this.intActivePage);
        console.log(this.arrPagination)
        this.isLoading = false;
      })
    }
  }

  onSearchMovieInput() {
    if (!this.strSearch.trim()) {
      this.isLoading = true;
      this.getMovie();
    }
  }

  onClickMovie(movieDetails: any) {
    this.router.navigate(['/home/details'], { queryParams: { movieId: movieDetails.id } })
  }
}
